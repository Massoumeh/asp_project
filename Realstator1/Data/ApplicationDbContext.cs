﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Realstator.Models;
using Realstator.Data.Configurations;


namespace Realstator.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<Home> Homes { get; set; }

        // Users is already defined in IdentityDbContext
        // public DbSet<IdentityUser> Users { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
        
    }
}
