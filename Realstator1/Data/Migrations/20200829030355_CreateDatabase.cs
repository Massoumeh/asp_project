﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Realstator.Data.Migrations
{
    public partial class CreateDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Articles");

            migrationBuilder.CreateTable(
                name: "Homes",
                columns: table => new
                {
                    HomeId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Addr = table.Column<string>(maxLength: 100, nullable: false),
                    Apt = table.Column<int>(nullable: false),
                    City = table.Column<string>(maxLength: 100, nullable: false),
                    Price = table.Column<decimal>(type: "Price", nullable: false),
                    Bdrms = table.Column<int>(nullable: false),
                    Bths = table.Column<int>(nullable: false),
                    Detail = table.Column<string>(maxLength: 150, nullable: true),
                    LDate = table.Column<DateTime>(nullable: false),
                    LSellerId = table.Column<string>(nullable: true),
                    SDate = table.Column<DateTime>(nullable: false),
                    SSellerId = table.Column<string>(nullable: true),
                    Paid = table.Column<decimal>(type: "Paid", nullable: false),
                    ImageName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Homes", x => x.HomeId);
                    table.ForeignKey(
                        name: "FK_Homes_AspNetUsers_LSellerId",
                        column: x => x.LSellerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Homes_AspNetUsers_SSellerId",
                        column: x => x.SSellerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Homes_LSellerId",
                table: "Homes",
                column: "LSellerId");

            migrationBuilder.CreateIndex(
                name: "IX_Homes_SSellerId",
                table: "Homes",
                column: "SSellerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Homes");

            migrationBuilder.CreateTable(
                name: "Articles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AuthorId = table.Column<string>(type: "TEXT", nullable: true),
                    Body = table.Column<string>(type: "TEXT", nullable: true),
                    CreationDateTime = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Title = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Articles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Articles_AspNetUsers_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Articles_AuthorId",
                table: "Articles",
                column: "AuthorId");
        }
    }
}
