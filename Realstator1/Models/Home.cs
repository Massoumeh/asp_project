using System;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Realstator.Models
{
    public class Home

    {
        public int HomeId { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Address cannot be longer than 100 characters.")]
        [Display(Name = "Address")]
        public string Addr { get; set; }
        public int Apt { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "City cannot be longer than 50 characters.")]
        [Display(Name = "City")]
        public string City { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "Price")]
        public decimal Price { get; set; }


        public enum Type { BUNGL, SPLIT, COTTG, SEMID, CONDO }


        public enum Pool { Yes, No }

        public int Bdrms { get; set; }

        public int Bths { get; set; }

        public enum Garage { Yes, No }

        [StringLength(150)]
        [Display(Name = "Detail")]
        public string Detail { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Listed Date")]
        public DateTime LDate { get; set; }

        public IdentityUser LSeller { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Soled Date")]
        public DateTime SDate { get; set; }
        public IdentityUser SSeller { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "Paid")]
        public decimal Paid { get; set; }

		public string ImageName { get; set; }

    }
}