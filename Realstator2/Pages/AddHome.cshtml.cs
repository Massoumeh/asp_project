using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Realstator.Data;
using Realstator.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.IO;

namespace Realstator.Pages
{
    [Authorize]
    public class AddHomeModel : PageModel
    {
        private ApplicationDbContext db;
    public AddHomeModel(ApplicationDbContext db) => this.db = db;


        public IdentityUser LSeller { get; set; }

        public string Addr { get; set; }
        public string AptNo { get; set; }

        public string City { get; set; }

        public decimal Price { get; set; }

        public Home.Type HomeType {get; set; }

        public bool Pool { get; set; }

        public int Bdrms { get; set; }

        public int Bths { get; set; }

        public bool Garage { get; set;}

        public string Detail { get; set; }

        public DateTime LDate { get; set; }

        public DateTime SDate { get; set; }
        public IdentityUser SSeller { get; set; }

        public decimal Paid { get; set; }

		[Required(ErrorMessage = "Please choose profile image")]  
        [Display(Name = "Profile Picture")]  
        public IFormFile ProfileImage { get; set; }  
        private readonly IWebHostEnvironment webHostEnvironment;  
        public AddHomeModel(ApplicationDbContext context, IWebHostEnvironment hostEnvironment)  
        {  
            db = context;  
            webHostEnvironment = hostEnvironment;  
        }  
  
        public async Task<IActionResult> Index()  
        {  
            var home = await db.Homes.ToListAsync();  
            return View(home);  
        }

        private IActionResult View(List<Home> home)
        {
            throw new NotImplementedException();
        }

        public IActionResult New()  
        {  
            return View();  
        }

        private IActionResult View()
        {
            throw new NotImplementedException();
        }

        [HttpPost]  
        [ValidateAntiForgeryToken]  
        public async Task<IActionResult> New(AddHomeModel model)  
        {  
            if (ModelState.IsValid)  
            {  
                string uniqueFileName = UploadedFile(model);  
  
                Home home = new Home  
                {  
                    Addr = model.Addr,  
                    City = model.City,
                    AptNo = model.AptNo,
                    Bdrms= model.Bdrms,  
                    Bths = model.Bths,  
                    LDate = model.LDate,  
                    Detail = model.Detail,  
                    HasGarage = model.Garage,  
                    HasPool = model.Pool,
                    Paid = model.Price,   
                    ProfilePicture = uniqueFileName,  
                };  
                db.Add(home);  
                await db.SaveChangesAsync();  
                return RedirectToAction(nameof(Index));  
            }  
            return View();  
        }

        private string UploadedFile(AddHomeModel model)  
        {  
            string uniqueFileName = null;  
  
            if (model.ProfileImage != null)  
            {  
                string uploadsFolder = Path.Combine(webHostEnvironment.WebRootPath, "images");  
                uniqueFileName = Guid.NewGuid().ToString() + "_" + model.ProfileImage.FileName;  
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);  
                using (var fileStream = new FileStream(filePath, FileMode.Create))  
                {  
                    model.ProfileImage.CopyTo(fileStream);  
                }  
            }  
            return uniqueFileName;  
        }  
    }
}    

