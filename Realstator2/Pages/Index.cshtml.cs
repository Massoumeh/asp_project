﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Realstator.Models;
using Realstator.Data;
using Microsoft.EntityFrameworkCore;

namespace Realstator.Pages
{
    [Authorize]
    public class IndexModel : PageModel
    {
        private readonly ApplicationDbContext db;  
        public IndexModel(ApplicationDbContext db) => this.db = db;
        public List<Home> Homes { get; set; } = new List<Home>();  
        public Home FeaturedHome { get; set; }  
        public async Task OnGetAsync()
        {
            Homes = await db.Homes.ToListAsync();
            FeaturedHome = Homes.ElementAt(new Random().Next(Homes.Count));
        }
    }
}
