﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Realstator.Data.Configurations;
using Realstator.Models;

namespace Realstator.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<Home> Homes { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=Realstator.db");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new HomeConfiguration());
             base.OnModelCreating(modelBuilder);
        }

    }
}
