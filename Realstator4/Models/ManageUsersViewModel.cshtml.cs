using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Realstator.Data;
using Realstator.Models;

namespace Realstator4.Pages
{
    public class ManageUsersViewModelModel : PageModel
    {
        public class ManageUsersViewModel
        {
            public string UserName { set; get; }
            public IEnumerable<RoleVm> Roles { set; get; }
            public int SelectedRole { set; get; }

            public string MyRadioField { get; set; }

        }
        public class RoleVm
        {
            public int Id { set; get; }
            public string RoleName { set; get; }
        }
        private ApplicationDbContext db;
        public ManageUsersViewModelModel(ApplicationDbContext db) => this.db = db;


        public string UserName { get; private set; }
        public string RoleName { set; get; }
        public IEnumerable<RoleVm> Roles { set; get; }

        public IActionResult OnPost()
        {
            ManageUsersViewModel muv = new ManageUsersViewModel();
            muv.UserName = UserName;
            RoleVm roleVm = new RoleVm();
            roleVm.RoleName = RoleName;
            if (ModelState.IsValid)
            {

                db.Add(muv);
                db.Add(roleVm);
                db.SaveChanges();

                return RedirectToPage();
            }
            return Page();
        }

    }

}