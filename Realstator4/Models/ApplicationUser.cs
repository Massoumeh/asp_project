using Microsoft.AspNetCore.Identity;  
  
namespace CustomUserData_Demo.Models  
{  
    public class ApplicationUser : IdentityUser  
    {  
       public enum Role { Admin, Seller, Buyer }

       
        public Role Roles {get; set; }
    }  
} 