using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Realstator.Data;
using Realstator.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace Realstator.Pages
{
    [Authorize]
    public class AddHomeModel : PageModel
    {
        private ApplicationDbContext db;
    public AddHomeModel(ApplicationDbContext db) => this.db = db;


        public IdentityUser LSeller { get; set; }

        public string Addr { get; set; }
        public int Apt { get; set; }

        public string City { get; set; }

        public decimal Price { get; set; }

        public Home.Type HomeType {get; set; }

        public bool Pool { get; set; }

        public int Bdrms { get; set; }

        public int Bths { get; set; }

        public bool Garage { get; set;}

        public string Detail { get; set; }

        public DateTime LDate { get; set; }

        public DateTime SDate { get; set; }
        public IdentityUser SSeller { get; set; }

        public decimal Paid { get; set; }

		public string ImageName { get; set; }
    public async Task<IActionResult> OnPostAsync()
    {
      if (ModelState.IsValid)
      {
        var userName = User.Identity.Name;
        //var user = from u in db.Users where u.UserName == userName select u;
        var user = db.Users.Where(u => u.UserName == userName).FirstOrDefault();
        var newHome = new Home { Addr = Addr, AptNo = Apt + "", LSeller = user, City = City, Price = Price, HomeType = HomeType, HasPool = Pool, Bdrms = Bdrms, HasGarage = Garage, Detail = Detail, ImageName = ImageName };

        newHome.LDate = DateTime.Now;
        db.Add(newHome);
        await db.SaveChangesAsync();

        return RedirectToPage("AddHomeSuccess");
      }
      return Page();
    }
    }
}
