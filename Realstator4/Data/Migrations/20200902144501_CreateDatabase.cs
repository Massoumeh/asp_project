﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Realstator.Data.Migrations
{
    public partial class CreateDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Home",
                columns: table => new
                {
                    HomeId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    LSellerId = table.Column<string>(nullable: true),
                    Addr = table.Column<string>(maxLength: 100, nullable: false),
                    AptNo = table.Column<string>(nullable: true),
                    City = table.Column<string>(maxLength: 100, nullable: false),
                    Price = table.Column<decimal>(type: "Price", nullable: false),
                    HomeType = table.Column<int>(nullable: false),
                    HasPool = table.Column<bool>(nullable: false),
                    Bdrms = table.Column<int>(nullable: false),
                    Bths = table.Column<int>(nullable: false),
                    HasGarage = table.Column<bool>(nullable: false),
                    Detail = table.Column<string>(maxLength: 150, nullable: true),
                    LDate = table.Column<DateTime>(nullable: false),
                    SDate = table.Column<DateTime>(nullable: false),
                    SSellerId = table.Column<string>(nullable: true),
                    Paid = table.Column<decimal>(type: "Paid", nullable: false),
                    ImageFileName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Home", x => x.HomeId);
                    table.ForeignKey(
                        name: "FK_Home_AspNetUsers_LSellerId",
                        column: x => x.LSellerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Home_AspNetUsers_SSellerId",
                        column: x => x.SSellerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Home_LSellerId",
                table: "Home",
                column: "LSellerId");

            migrationBuilder.CreateIndex(
                name: "IX_Home_SSellerId",
                table: "Home",
                column: "SSellerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Home");
        }
    }
}
