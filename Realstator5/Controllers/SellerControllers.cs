using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Realstator.Controllers
{
    public class SellerController : Controller
    {
        [Authorize(Roles = "Seller")]
        public IActionResult Index()
        {
            return View();
        }
    }
}