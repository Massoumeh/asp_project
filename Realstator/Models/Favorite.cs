using System;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Realstator.Models
{
    public class Favorite

    {    

		[Key]       
	    public int HomeId { get; set; }

		public AppUser UserId { get; set; }  

		public Home Home { get; set; }
		public ICollection<AppUser> Users { get; set; }
		
    }
}