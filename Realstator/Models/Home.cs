using System;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Realstator.Models
{
    public class Home

    {
        public int HomeId { get; set; }

        public AppUser LSeller { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Address cannot be longer than 100 characters.")]
        [Display(Name = "Address")]
        public string Addr { get; set; }
        public string AptNo { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "City cannot be longer than 50 characters.")]
        [Display(Name = "City")]
        public string City { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "Price")]
        public decimal Price { get; set; }
        
        public enum Type { None = -1, BUNGL = 0, SPLIT = 1, COTTG = 2, SEMID = 3, CONDO = 4 }
        [Required]
        public Type HomeType {get; set; }

        public bool HasPool {get; set; }

        public int Bdrms { get; set; }

        public int Bths { get; set; }

        public bool HasGarage { get; set;}

        [StringLength(150)]
        [Display(Name = "Detail")]
        public string Detail { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Listed Date")]
        public DateTime LDate { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Soled Date")]
        public DateTime SDate { get; set; }
        public AppUser SSeller { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "Paid")]
        public decimal Paid { get; set; }

        [Required(ErrorMessage = "Please choose profile image")]  
        public string ProfilePicture { get; set; }  

    }
}