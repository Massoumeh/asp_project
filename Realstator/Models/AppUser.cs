using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
  
namespace Realstator.Models
{
    public class AppUser: IdentityUser
    {
        [Display(Name = "First Name")]        
        [StringLength(30)]

        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [StringLength(30)]
        public string LastName { get; set; }

        public enum Role { Admin, Seller, Buyer}
        [Required]
        public Role AppUserRole {get; set; }

    }
}