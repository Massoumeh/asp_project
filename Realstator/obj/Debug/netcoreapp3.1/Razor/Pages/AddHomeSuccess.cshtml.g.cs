#pragma checksum "C:\Users\Mmaha\Documents\ASP_Project\Realstator\Pages\AddHomeSuccess.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "260d6ca6e390e243e241e7fbda7df5bde37b5c81"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(Realstator.Pages.Pages_AddHomeSuccess), @"mvc.1.0.razor-page", @"/Pages/AddHomeSuccess.cshtml")]
namespace Realstator.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Mmaha\Documents\ASP_Project\Realstator\Pages\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Mmaha\Documents\ASP_Project\Realstator\Pages\_ViewImports.cshtml"
using Realstator;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Mmaha\Documents\ASP_Project\Realstator\Pages\_ViewImports.cshtml"
using Realstator.Data;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"260d6ca6e390e243e241e7fbda7df5bde37b5c81", @"/Pages/AddHomeSuccess.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"09d83a6381c0e058febc651148c288670b743114", @"/Pages/_ViewImports.cshtml")]
    public class Pages_AddHomeSuccess : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 3 "C:\Users\Mmaha\Documents\ASP_Project\Realstator\Pages\AddHomeSuccess.cshtml"
  
    ViewData["Title"] = "Added New Property";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h1>Property Added successfully</h1>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Realstator.Pages.AddHomeSuccessModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<Realstator.Pages.AddHomeSuccessModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<Realstator.Pages.AddHomeSuccessModel>)PageContext?.ViewData;
        public Realstator.Pages.AddHomeSuccessModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
