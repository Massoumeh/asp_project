using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Realstator.Data;
using Realstator.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Runtime.Serialization;

namespace Realstator.Pages
{
    [Authorize(Roles="Seller,Admin")]
    public class AddHomeModel : PageModel
    {
        private ApplicationDbContext db;
        private UserManager<AppUser> _userManager;
        private readonly IWebHostEnvironment _env;

        public AddHomeModel(ApplicationDbContext db, UserManager<AppUser> userManager, IWebHostEnvironment environment)
        {
            this.db = db;
            _userManager = userManager;
            _env = environment;
        }

        [Required(ErrorMessage = "Please choose profile image")]  
        [Display(Name = "Profile Picture")]  
        public IFormFile ProfileImage { get; set; } 

        [BindProperty]
        public string Addr { get; set; }

        [BindProperty]
        public string AptNo { get; set; }

        [BindProperty]

        public string City { get; set; }

        [BindProperty]
        public decimal Price { get; set; }

        [BindProperty]
        public Home.Type HomeType { get; set; }

        [BindProperty]
        public bool Pool { get; set; }

        [BindProperty]
        public int Bdrms { get; set; }
        
        [BindProperty]
        public int Bths { get; set; }

        [BindProperty]
        public bool Garage { get; set; }

        [BindProperty]
        public string Detail { get; set; }

        [BindProperty]
        public DateTime LDate { get; set; }

        // [BindProperty]
        // public string ImageName { get; set; }


        public async Task<IActionResult> OnPostAsync()
        
        {
            
            var file = Path.Combine(_env.ContentRootPath, "uploads", ProfileImage.FileName);
            using (var fileStream = new FileStream(file, FileMode.Create))
            {
                await ProfileImage.CopyToAsync(fileStream);
            }
            if (ModelState.IsValid)
            {
                var userName = User.Identity.Name;
                var user = await _userManager.FindByNameAsync(userName);
                //var user = (from u in db.Users where u.UserName == userName select u).FirstOrDefault();
                //var user = db.Users.Where(u => u.UserName == userName).FirstOrDefault();
                var newHome = new Home { Addr = Addr, AptNo = AptNo, City = City, Price = Price, HomeType = HomeType, HasPool = Pool, Bdrms = Bdrms, Bths = Bths, HasGarage = Garage, Detail = Detail, LDate = DateTime.Now, ProfilePicture = ProfileImage.FileName };
                db.Add(newHome);
                db.SaveChanges();

                return RedirectToPage("AddHomeSuccess");
            }
            return Page();
        }

    }
}
