using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Realstator.Data;
using Realstator.Models;

namespace Realstator.Pages
{

    public class AddHomeSuccessModel : PageModel
    {
        private ApplicationDbContext db;

        public AddHomeSuccessModel(ApplicationDbContext db) => this.db = db;
        [BindProperty(SupportsGet = true)]
        public int Id { get; set; }
        public Home Home { get; set; }

        public async Task OnGetAsync() => Home = await db.Homes.FindAsync(Id);

    }

}
